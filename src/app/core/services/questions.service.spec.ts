import { TestBed } from '@angular/core/testing';

import { QuestionsService } from './questions.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('QuestionsService', () => {
  let service: QuestionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers:[HttpClient, HttpHandler]});
    service = TestBed.inject(QuestionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
