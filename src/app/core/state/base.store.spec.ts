import { TestBed } from '@angular/core/testing';

import { BaseStore } from './base.store';
import { QuestionModel } from './question.model';

describe('BaseService', () => {
  let service: BaseStore<boolean, QuestionModel>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: BaseStore,
          useValue: new BaseStore<boolean, QuestionModel>(true),
        },
      ],
    });

    service = TestBed.inject(BaseStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
