import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';
import { QuestionModel } from 'src/app/core/state/question.model';
import { QuestionsService } from 'src/app/core/services/questions.service';
import { ActivatedRoute } from '@angular/router';

const mockQuestions: QuestionModel[] = [
  {
    _id: '1',
    question: 'What is the capital of France?',
    category: '',
    difficulty: '',
    type: '',
    answers: [
      {
        _id: '1',
        text: 'Paris',
        isCorrect: true,
      },
      {
        _id: '2',
        text: 'Madrid',
        isCorrect: false,
      },
    ],
    selectedId: '',
  },
];

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  let questionsServiceSpy: jasmine.SpyObj<QuestionsService>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;

  beforeEach(async () => {
    const questionsService = jasmine.createSpyObj('QuestionsService', [
      'getQuestions',
      'gameComplete',
    ]);
    const activatedRoute = jasmine.createSpyObj('ActivatedRoute', [], {
      queryParams: of({
        type: 'multiple',
        amount: '1',
        difficulty: 'easy',
      }),
    });

    await TestBed.configureTestingModule({
      declarations: [PlayComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [
        { provide: QuestionsService, useValue: questionsService },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    questionsServiceSpy = TestBed.inject(
      QuestionsService
    ) as jasmine.SpyObj<QuestionsService>;
    activatedRouteSpy = TestBed.inject(
      ActivatedRoute
    ) as jasmine.SpyObj<ActivatedRoute>;

    questionsServiceSpy.getQuestions.and.returnValue(of(mockQuestions));
    questionsServiceSpy.gameComplete.and.returnValue(of(1));

    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    component.questions$ = of([
      <QuestionModel>{
        question: 'What is the capital of Brazil?',
        answers: [
          { _id: '1', text: 'Brasilia', isCorrect: true },
          { _id: '2', text: 'Berlin' },
          { _id: '3', text: 'Madrid' },
          { _id: '4', text: 'Buenos Aires' },
        ],
      },
    ]);
    fixture.detectChanges();
  });

  afterEach(() => {
    component.getQuestionsSubscription.unsubscribe();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should color selected answer', () => {
    const answerButton = fixture.nativeElement.querySelector(
      '.answers button:first-child'
    );
    expect(answerButton.classList.contains('bg-primary-default')).toBe(true);
    answerButton.click();
    fixture.detectChanges();
    expect(answerButton.classList.contains('bg-secondary-default')).toBe(true);
  });

  it('should display error message when API request fails', () => {
    questionsServiceSpy.getQuestions.and.returnValue(throwError('Error'));

    expect(component.errorMsg).toEqual('');
    component.ngOnInit();
    expect(component.errorMsg).toEqual('Could not reach the api.');
    fixture.detectChanges();
    const errorMsg = fixture.nativeElement.querySelector('.errorTxt');
    expect(errorMsg.textContent).toContain('Could not reach the api.');
  });
});
